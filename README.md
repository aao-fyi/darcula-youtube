# Darcula YouTube

A dark YouTube theme for [Stylus](https://github.com/openstyles/stylus); enhance your video-watching experience. This style creates a darker aesthetic while still providing some color and accents that match the YouTube theme.

![screenshot](screenshot.jpg)
